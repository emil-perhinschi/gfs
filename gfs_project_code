#!/bin/sh
# gfs_project_code - Set or retrieve the current project

# Stop the script if any command exits with non-zero exit status and
# isn't caught (e.g. by && or ||).
set -e

# error "error string"
# Given a string, outputs the string and exits the script with a non-zero
# status.  Used to report errors.  The script will exit before error exits
# with non-zero status, and set -e above will then abort the script.
#
# Example
# do_something || error "something failed!"
#
error() {
    echo "$@" >&2
    exit 1
}

USAGE="Usage: $(basename "$0") [-r] [project_code]"

# Get the root directory of the git repo we're in, if we're in one.
repo=$(git rev-parse --show-toplevel 2>/dev/null) && repo_name="$(basename "$repo")" || repo_name=""
mkdir -p "$HOME/.gfs"
code_cache_file="$HOME/.gfs/current_projects"

if test "$1" = "-r" ; then
    grep -E "^$repo_name:" "$code_cache_file" | cut -d: -f2
elif test "$1" ; then
    project_record="$repo_name:$1"
    if grep -E "^$repo_name:" "$code_cache_file" 1>/dev/null 2>&1 ;  then
        # Take out the project record for the repo
        grep -E -v "^$repo_name:" "$code_cache_file" > "$code_cache_file.bkp" || :
        mv "$code_cache_file.bkp" "$code_cache_file"
    fi

    # Add the project record in
    echo "$project_record" >> "$code_cache_file"
else
    error "$USAGE"
fi


exit 0
:  <<'END_OF_DOCS'
=head1 NAME

gfs_project_code - Set or return the current project code

=head1 SYNOPSIS

    # Tell gfs you're on the "APP" team
    gfs project_code APP

    # Find out what project gfs thinks you're working on
    gfs project_code -r

=head1 DESCRIPTION

gfs project_code provides a centralized place to get and set the project_code
on which the other gfs scripts rely.  This code is used to determine, for
example, which config file or hooks to use when connecting to JIRA,
committing code, etc.  So, it's quite important that you have the correct
project code set when running gfs commands.

The project_code is stored and retrieved along with the name of the repository
you're in at the time you run the command.  That means you can switch between
projects within a repo, or switch between repos that use different projects.

For example, maybe you work on several small projects and each repo is
mapped to a JIRA project.  You can cd into repo1 and gfs project_code APP1,
then cd into repo2 and "gfs project_code APP2".  If you cd back into repo1
and run "gfs project_code -r", you'll get "APP1".

The codes are stored in $HOME/.gfs/current_projects, which means it doesn't
touch your git repos.  You can easily and safely manually edit that file if
you need to for some nefarious reason (e.g. to delete a project code).

In a good development environment, if you're working on a large repository,
you'll have teams mapped to areas of the code (e.g. by feature area).
Each team might have their own JIRA "board" (usually also associated with
a JIRA "project" - in JIRA terms, "product" or "team" would probably be
a better word than "project", but then I didn't write JIRA.
In this case, you cd into your repository, and type "gfs project_code APP",
where "APP" is the JIRA project code for the team you're working on.
If you switch teams, to say the "ARG" team, "gfs project_code ARG" will take
you to that team and make sure you're working with the right config settings.
If the config settings are saved in the git repo (as they should be if
your teams are using gfs), you won't need to do any setup.  If your teams
aren't using gfs, "gfs init ARG" will get you set up, and hopefully make your
team jealous.  It's up to you whether to run "gfs init" in your home dir
or in the git repo.

If, conversely, you're working on several smaller projects, perhaps with one
JIRA project per repo (which is what you should do), you can just run
C<gfs project_code> once in each repo (on each workstation you use) and
C<gfs> will remember which project code you use for each repo.

Note that you should have the following relationships set up for a good
development environment:

=over

=item One repository per product

If you have a big product, you might have several repositories, but they should
have very clear boundaries (e.g. documented APIs) between them and you should
*NOT* develop interdependent features between repositories.  That is, if
you're adding a feature to repo1 and it depends on functionality that doesn't
exist yet in repo2, then you must wait until the functionality has been
added to repo2 before starting development on the feature in repo1.

=item One or more JIRA "projects" per repository

For smaller products, you'd have one JIRA "project" per repository.  As
your product grows, you may have more than one "project" - e.g. one for
each major feature area in your application.  You should not have more
than one repository per JIRA project.

=item One or more JIRA projects per Scrum team

In larger environments, you'll want one Scrum team to work on one JIRA project,
which is for one area of a product/repository.

In smaller environments, you might have one team with a "board" that spans
multiple projects.  That's fine.  You should avoid having multiple teams
on a single project, however, or you're likely to run into miscommunication,
git conflicts, and buggy code.

=item One team per area of code in each repository

Your teams need to get to know, and be responsible for, an area of code.
That can be an entire repository, or a set of directories.  This gives you
the ability to easily tell who is responsible for what - in the end,
what your entire software development team is doing is editing text files.
Make sure each team is responsible for a certain set of those files.

=back

=cut
END_OF_DOCS
