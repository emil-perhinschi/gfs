#!/usr/bin/env bash
# Set up test data before any given set of test runs

set -e

./test_teardown.sh # Start with a clean slate

cd /tmp
test -d gfs_test_repo || mkdir gfs_test_repo
cd gfs_test_repo
echo "This is a test file." > test_file
echo "Test is also a test file." > also_test_file
git init
git config user.email "testuser@example.com"
git config user.name "Joe Tester"
git add -A
git commit -m "Committing test files"

