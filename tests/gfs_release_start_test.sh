#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

setupPrefixlessConfig() {
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
release_start_status:2
develop_branch:master
branch_field:tracker_branch_field
" > /tmp/gfs_test_repo/.gfs/TEST/config
}

setupConfigWithPrefix() {
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
release_start_status:2
develop_branch:master
branch_field:tracker_branch_field
release_branch_prefix:release/
" > /tmp/gfs_test_repo/.gfs/TEST/config
}

oneTimeSetUp() {
    ./test_setup.sh

    setupPrefixlessConfig

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    test "$current_branch_name" = "master" || git checkout master
}

tearDown() {
    test -f testfile && { git rm testfile ; git commit -m "Cleanup" ; }

    # Get us back to master if we're not already there
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    test "$current_branch_name" = "master" || git checkout master

    # Delete the "test_release" branch if it exists
    git rev-parse --verify test_release >/dev/null 2>&1 && \
        git branch -D test_release
    cd "$SCRIPTDIR" || exit 1
}

testMakesNewBranch() {
    gfs release start -t "RTEST-1" -b "test_release"
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    assertEquals "test_release" "$current_branch_name"
    git checkout master
    result=$?
    message="Should be able to check out master branch after starting release"
    assertTrue "$message" $result
    git branch -D test_release
}

testUpdatesTrackerStatus() {
    gfs release start -t "RTEST-1" -b "test_release"
    output=$(grep -E '^status:' /tmp/gfs_tracker_mock-RTEST-1)
    assertEquals "status:In Development" "$output"
}

testUpdatesBranchInTracker() {
    gfs release start -t "RTEST-1" -b "test_release"
    output=$(grep -E '^branch_name' /tmp/gfs_tracker_mock-RTEST-1)
    assertEquals "branch_name:test_release" "$output"
}

testAbortsIfHookFails() {
    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/release_start
    chmod 755 .gfs/TEST/hooks/release_start

    # Start the release
    if gfs_release_start -t "RTEST-1" -b "test_release" ; then
        fail "gfs_release_start should fail if its hook fails"
    else
        assertTrue 0
    fi

    # Clean up
    rm .gfs/TEST/hooks/release_start
}

testChecksOutDevelopBranchIfHookFails() {
    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/release_start
    chmod 755 .gfs/TEST/hooks/release_start

    # Start the release
    gfs_release_start -t "RTEST-1" -b "test_release"

    # Make sure we're on the "develop" branch (which we've configured to be
    # "master" in oneTimeSetup)
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    assertEquals "master" "$current_branch_name"

    # Clean up
    rm .gfs/TEST/hooks/release_start
}

testCreatesBranchWithPrefix() {
    # Add the config file that has the release_branch_prefix param set to
    # "release/"
    setupConfigWithPrefix
    gfs_release_start -t "RTEST-1" -b "prefix_release_test"
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)

    assertEquals "release/prefix_release_test" "$current_branch_name"

    # Clean up
    git checkout master
    git branch -D "release/prefix_release_test"
    setupPrefixlessConfig
}

testPullsDevelopIfThereIsARemote() {
    git init --bare /tmp/gfs_test_repo_remote
    git remote add origin /tmp/gfs_test_repo_remote
    git push --set-upstream origin master
    output=$(gfs release start -t "TEST-1" -b "test_release" 2>&1)
    if echo "$output" | grep -E "Already.up.to.date" > /dev/null ; then
        assertTrue "${SHUNIT_TRUE}"
    else
        echo "output from feature start: $output"
        message="gfs release start should try to pull remote develop branch "
        message+="if a remote repo exists"
        fail "$message"
    fi
    rm -rf /tmp/gfs_test_repo_remote
    git remote remove origin
}

testDoesntPullDevelopIfThereIsNoRemote() {
    output=$(gfs release start -t "TEST-1" -b "test_release" 2>&1)
    if echo "$output" | grep -E "Already.up.to.date" > /dev/null ; then
        message="gfs release start should try to pull remote develop branch "
        message+="if a remote repo exists"
        fail "$message"
    else
        assertTrue "${SHUNIT_TRUE}"
    fi
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2
