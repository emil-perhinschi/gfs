#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
in_development_status:2
develop_branch:master
branch_field:tracker_branch_field
" > /tmp/gfs_test_repo/.gfs/TEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd "$SCRIPTDIR" || exit 1
}

testErrorsIfTicketNotProvided() {
    gfs commit -m "I forgot to include a ticket"
    assertFalse "gfs commit should error if -t not provided" $?
}

testPrependsTicketToCommitMessage() {
    output=$(gfs commit USE_MOCK_GIT_FOR_TESTING -t "ABC-123" -m "Make changes")
    assertContains "$output" "git mock -m:ABC-123 Make changes"
}

testPassesArgumentsToGit() {
    output=$(gfs commit USE_MOCK_GIT_FOR_TESTING -t "ABC-123" --argue --blah)
    assertContains "$output" "git mock -m:ABC-123"
    assertContains "$output" "git mock --argue"
    assertContains "$output" "git mock --blah"
}

testAssignsTicketToUser() {
    gfs commit USE_MOCK_GIT_FOR_TESTING -t "ABC-123" -m "Make changes"

    mock_output=$(cat /tmp/gfs_tracker_mock-ABC-123)
    mock_username=$(gfs tracker "$(gfs config read issue_tracker)" username)

    # Note: these come from the config set in oneTimeSetUp
    assertContains "$mock_output" "command:update"
    assertContains "$mock_output" "assignee:$mock_username"
    assertContains "$mock_output" "status:In Development"
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2
