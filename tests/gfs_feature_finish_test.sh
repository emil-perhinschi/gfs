#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
feature_done_status:3
develop_branch:master
" > /tmp/gfs_test_repo/.gfs/TEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
    gfs feature start -t "TEST-1" -b "test_feature"
    echo "I made some changes" > testfile
    git add testfile
    git commit -m "TEST-1 made changes"
}

tearDown() {
    test -f testfile && { git rm testfile ; git commit -m "Cleanup" ; }
    git checkout master
    git branch -D test_feature
    cd "$SCRIPTDIR" || exit 1
}

testUpdatesIssueToDone() {
    echo "Testing: Updates issue to Done"
    gfs feature finish -t "TEST-1"
    updates_done=$(grep -E '^status:' /tmp/gfs_tracker_mock-TEST-1)
    assertEquals "status:Done" "$updates_done"
}

testPullsIfThereIsARemote() {
    echo "Testing: Pulls if there's a remote branch"
    git init --bare /tmp/gfs_test_repo_remote
    git remote add origin /tmp/gfs_test_repo_remote
    git push --set-upstream origin test_feature
    if gfs feature finish -t "TEST-15" 2>&1 | grep "git pull" > /dev/null ; then
        assertTrue 0
    else
        fail "gfs feature finish should try to pull remote branches if a remote repo exists"
    fi
    rm -rf /tmp/gfs_test_repo_remote
    git remote remove origin
}

testAbortsIfMergeFails() {
    echo "Testing: Aborts if merge fails in $(pwd)"
    git checkout master
    echo "Branch:"
    git branch
    echo "I made some conflicting changes" > testfile
    git add testfile
    git commit -m "TEST-2 made conflicting changes"
    git checkout test_feature

    # Make sure it aborts
    if gfs feature finish -t "TEST-2" ; then
        fail "gfs_feature_finish should fail and abort if there's a conflict"
    else
        assertTrue 0
    fi

    # Make sure it didn't leave the repo in a merge state
    git merge HEAD &> /dev/null
    result=$? # 0 means the merge was aborted, 128 means repo is in a merge
    assertEquals "gfs feature finish should abort merge" "$result" "0"

}

testAbortsIfHookFails() {
    echo "Testing: Aborts if hook fails"

    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/feature_finish
    chmod 755 .gfs/TEST/hooks/feature_finish

    # Finish the feature
    if gfs_feature_finish -t "TEST-3" ; then
        fail "gfs_feature_finish should fail if its hook fails"
    else
        assertTrue 0
    fi

}

testChecksOutOriginalBranchIfHookFails() {
    :
}

oneTimeTearDown() {
    ./test_teardown.sh

    rm -rf ~/.gfs/TESTCONFIG
}

# shellcheck disable=SC1091
. ./shunit2
