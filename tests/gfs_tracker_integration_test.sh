#!/usr/bin/env bash

# Don't blow up automated tests - just exit if called w/no arguments
[ $# -eq 0 ] && exit 0

USAGE="usage: $(basename "$0") [-c config_file] -t test_ticket_id"

while getopts hvc:t: OPT; do
    case "$OPT" in
        h)  # help
            perldoc "$0"
            exit 0
            ;;
        v) # script version
            echo "$(basename "$0") version 0.1"
            exit 0
            ;;
        c) # Config file
            CONFIG_FILE="$OPTARG"
            ;;
        t) # Test ticket (aka "issue" in most systems)
            ISSUE="$OPTARG"
            ;;
        \?) # Unexpected argument - usage error
            # getopts issues an error message
            echo "$USAGE" >&2
            exit 1
            ;;
    esac
done

# Remove the switches we parsed above.
shift $((OPTIND - 1))

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

TRACKER=""
USERNAME=""

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/INTEGRATIONTEST
    cat "$CONFIG_FILE" > /tmp/gfs_test_repo/.gfs/INTEGRATIONTEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code INTEGRATIONTEST

    # Only set this once
    TRACKER=$(gfs config read issue_tracker)
    USERNAME=$(gfs tracker "$TRACKER" username)

    # Go back to where the tests are
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd "$SCRIPTDIR" || exit 1
}

testReturnsUsername() {
    username=$(gfs tracker "$TRACKER" username)
    assertNotNull "\"gfs tracker $TRACKER username\" should return a user ID" \
        "$username"
}

testUpdatesStatus() {
    # Get the first available status. We call them "transitions" because
    # Jira was the first tracker and it has transitions.
    first_transition=$(gfs tracker "$TRACKER" statuses -t "$ISSUE" | \
        head -1)

    # `gfs tracker "$TRACKER" statuses -t "$ISSUE"` returns a list that
    # includes an status ID (what should be sent to the tracker to change
    # the status) and a status Name (what to display to a person). In Jira,
    # the status ID is actually a "transition ID" (an integer), which describes a
    # "transition" from one status to another. That means that, for example, to
    # get to "In Development" from "To Do" might be a different transition ID
    # than to get to "In Development" from "Done". "statuses" returns the
    # available statuses, and what to send to get to them, for the current
    # issue at the current time.
    should_have="gfs tracker \"$TRACKER\" statuses -t $ISSUE should return "
    should_have+="a list of one or more available statuses"
    assertNotNull "$should_have" "$first_transition"
    transition_id=$(echo "$first_transition" | cut -d: -f1)
    transition_name=$(echo "$first_transition" | cut -d: -f2)

    # Transition to it
    gfs tracker "$TRACKER" update -t "$ISSUE" -s "$transition_id"

    # Read the status
    status=$(gfs tracker "$TRACKER" read -t "$ISSUE" -s | cut -d: -f2)

    # Make sure we got the right status. Note that this isn't always reliable,
    # as the transition name might not be the same as the status.
    assertEquals "$transition_name" "$status"
}

testReadsStatus() {
    # Make sure it returns a status code, not a JSON string. We just check for
    # one or more letters, and no commas.
    # Note that whether this is the "id" or "name" of the status is up to the
    # tracker.
    # A tracker *should* accept either a name or the ID when updating the
    # status, so that the value returned by "read -s" can be passed to "update
    # -s". That's not actually enforced by a script (scripts use values in the
    # config file), so we don't test for it here - just make sure we get
    # something back that looks like a status.
    gfs tracker "$TRACKER" read -t "$ISSUE" -s | \
        grep -E '^status:[a-zA-Z0-9]+' | grep -v ',' > /dev/null
    result=$?
    assertTrue $result
}

testUpdatesAssignee() {
    # Unassign the issue
    gfs tracker "$TRACKER" update -t "$ISSUE" -a "UNASSIGNED"
    result=$?
    assertTrue "Unassigning the issue shouldn't fail" $result

    # Read the assignee - make sure it's unassigned. Yes this duplicates
    # the testUnassignsTicketUsingUnassigned test, but is handy if
    # testUpdatesAssigneeToMe is failing.
    # shellcheck disable=SC2034
    output=$(gfs tracker "$TRACKER" read -t "$ISSUE" -a | grep -E '^assignee:')
    # shellcheck disable=SC2016
    assertEquals "$output" "assignee:"

    # Now assign it to the user explicitly
    gfs tracker "$TRACKER" update -t "$ISSUE" -a "$USERNAME"
    output=$(gfs tracker "$TRACKER" read -t "$ISSUE" -a | \
        grep -E '^assignee:')
    # Not redundant - $USERNAME could, but shouldn't, be empty
    assertNotEquals "$output" 'assignee:'
    assertEquals "$output" "assignee:$USERNAME"
}

testReadsAssignee() {
    # Test basic reading - write and read is handled in testUpdatesAssignee
    output=$(gfs tracker "$TRACKER" read -t "$ISSUE" -a | \
        grep -E '^assignee:')
    result=$?
    assertTrue "\"gfs tracker $TRACKER read -t $ISSUE -a\" should succeed" \
        $result
    assertContains "$output" 'assignee:'
}

testUnassignsTicketUsingUnassigned() {
    # Given a tracker and a test ISSUE
    # When the assignee is set to all-caps "UNASSIGNED"
    # The issue is unassigned.

    # Unassign the issue
    gfs tracker "$TRACKER" update -t "$ISSUE" -a "UNASSIGNED"
    result=$?
    assertTrue "Unassigning the issue shouldn't fail" $result

    # Read the assignee - make sure it's unassigned ({"assignee":null})
    # shellcheck disable=SC2034
    output=$(gfs tracker "$TRACKER" read -t "$ISSUE" -a | grep -E '^assignee:')
    # shellcheck disable=SC2016
    assertEquals "$output" "assignee:"
}

testUpdatesAssigneeToMe() {
    # Unassign the issue
    gfs tracker "$TRACKER" update -t "$ISSUE" -a "UNASSIGNED"
    result=$?
    assertTrue "Unassigning the issue shouldn't fail" $result

    # Read the assignee - make sure it's unassigned. Yes this duplicates
    # the testUnassignsTicketUsingUnassigned test, but is handy if
    # testUpdatesAssigneeToMe is failing.
    # shellcheck disable=SC2034
    output=$(gfs tracker "$TRACKER" read -t "$ISSUE" -a | grep -E '^assignee:')
    # shellcheck disable=SC2016
    assertEquals "$output" "assignee:"

    # Now assign it to ME
    gfs tracker "$TRACKER" update -t "$ISSUE" -a "ME"
    output=$(gfs tracker "$TRACKER" read -t "$ISSUE" -a | \
        grep -E '^assignee:')
    assertNotContains "$output" 'assignee:ME'
    # Not redundant - $USERNAME could be empty
    assertNotEquals "$output" 'assignee:'
    assertEquals "$output" "assignee:$USERNAME"
}

testUpdatesBranchName() {
    # Given a branch name
    branch_name="my_test_branch"
    # When gfs tracker <tracker> update -t ticket_id -b "branch_name" is called
    gfs tracker "$TRACKER" update -t "$ISSUE" -b "$branch_name"
    result=$?
    assertTrue "Updating branch name should exit with status 0" $result

    # Then `gfs tracker <tracker> read -b` returns "ticket_id-branch_name"
    read_branch_name=$(gfs tracker "$TRACKER" read -t "$ISSUE" -b | \
        grep -E 'branch_name:' | cut -d: -f2)

    assertEquals "$branch_name" "$read_branch_name"
}

testReadsBranchName() {
    # Then `gfs tracker <tracker> read -b` returns "ticket_id-branch_name"
    read_branch_name=$(gfs tracker "$TRACKER" read -t "$ISSUE" -b)
    result=$?
    assertTrue "Reading branch name should exit with status 0" $result

    assertContains "$read_branch_name" "branch_name:"
}

testAddsComment() {
    gfs tracker "$TRACKER" update -t "$ISSUE" -c "This is a test comment"
    result=$?
    assertTrue "Adding a comment shouldn't fail" $result
}

testSupportsTestCommand() {
    # Given a valid tracker login information
    # When gfs tracker $TRACKER test is called
    rc=0 ; gfs tracker "$TRACKER" test || rc=$?

    # Then it returns a 0 exit code
    assertTrue "gfs tracker $TRACKER should support test command" $rc

}

testTestCommandFailsWithBadLogin() {
    # Given an invalid tracker login
    # When gfs tracker $TRACKER test is called
    rc=0 ; gfs tracker "$TRACKER" test bad_connection || rc=$?

    # Then it returns a non-zero exit code
    assertFalse "gfs tracker $TRACKER test bad_connection should error" $rc
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2

# Docs. Read by runing perldoc on this file
: <<END_OF_DOCS
=head1 NAME

gfs_tracker_integration_test.sh - Test a gfs tracker implementation

=head1 SYNOPSIS

    gfs_tracker_integration_test.sh \
        -c config_file \
        -t test_ticket_id

=head1 OPTIONS

=over

=item -c config_file

The path to a config file. This only needs to
contain the "issue_tracker" and "branch_name" parameters, plus any parameters
specific to your tracker.

This config file will run the integration tests on the mock test script
(gfs_tracker_mock):

    issue_tracker:mock
    branch_field:branch_name

You can put that in a file, e.g. "mock_config" and run
gfs_tracker_integration_test.sh on it like so:

    ./gfs_tracker_integration_test.sh -c mock_config -t TEST-123

C<gfs_tracker_integration_test.sh> determines which issue tracker to test by
reading the "issue_tracker" parameter from the config file.

C<gfs_tracker_integration_test.sh> will create a test project named
"INTEGRATIONTEST" in the test git repository it sets up. If you happen to have
a project of the same name in C<~/.gfs>, your home directory config settings
will override the test ones.

=item -t test_ticket_id

The identifier of a ticket that can be used for testing.
C<gfs_tracker_integration_test.sh> doesn't create tickets, so you'll need to
provide one that can be updated safely. C<gfs_tracker_integration_test.sh> will
make destructive updates to this issue, including adding a comment and updating
the assignee, status, and branch fields, so I recommend that you create a new
issue exclusively for testing.

=back

=head1 DESCRIPTION

Runs a set of tests on the specified issue tracker. You can use this if you're
writing a new gfs_tracker script to make sure your tracker supports all the
necessary options properly.

=cut
END_OF_DOCS

