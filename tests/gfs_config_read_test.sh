#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p ~/.gfs/TESTCONFIG
    mkdir -p /tmp/gfs_test_repo/.gfs/TESTCONFIG
    echo "onlyhome:homevalue" >> ~/.gfs/TESTCONFIG/config
    echo "inboth:bothhomevalue" >> ~/.gfs/TESTCONFIG/config
    {
        echo "inboth:bothrepovalue"
        echo "onlyrepo:repovalue"
        echo "issue_tracker:mock"
        echo "in_development_transition:12"
        echo "jira_domain:test.nothing.qqq"
    } >> /tmp/gfs_test_repo/.gfs/TESTCONFIG/config

}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd "$SCRIPTDIR" || exit 1
}

readConfigInRepo() {
    gfs config read -p TESTCONFIG "$@"
}

testReturnsHomeDirValueIfOnlyInHomeDir() {
    value=$(readConfigInRepo onlyhome)
    assertEquals "Should have returned config value from home dir." \
        "homevalue" "$value"
}

testReturnsRepoValueIfOnlyInRepo() {
    value=$(readConfigInRepo onlyrepo)
    assertEquals "Should have returned value from repo." "repovalue" "$value"
}

testReturnsHomeDirValueIfInBoth() {
    value=$(readConfigInRepo inboth)
    assertEquals "Should have overridden repo value with home dir value." \
        "bothhomevalue" "$value"
}

testReturnsNothingIfConfigFieldNotFound() {
    value=$(readConfigInRepo notafield)
    assertEquals "Should have returned nothing and not errored." "" "$value"
}

testReturnsSetIssueTracker() {
    issue_tracker=$(readConfigInRepo issue_tracker)
    assertEquals "mock" "$issue_tracker"
}

testOutputsDeprecationWarningForInDevelipmentTransition() {
    warnings=$(readConfigInRepo in_development_transition 1>/dev/null 2>&1)
    assertContains "WARNING" "$warnings"
    assertContains "update" "$warnings"

    value=$(readConfigInRepo in_development_status)
    assertEquals "12" "$value"
}

testTrackerDomainFallsBackToJiraDomainWithWarning() {
    warnings=$(readConfigInRepo tracker_domain 1>/dev/null 2>&1)
    assertContains "WARNING" "$warnings"
    assertContains "update" "$warnings"

    value=$(readConfigInRepo tracker_domain)
    assertEquals "test.nothing.qqq" "$value"
}

testIssueTrackerDefaultsToJira() {
    mkdir -p /tmp/gfs_test_repo/.gfs/TESTDEFAULTCONFIG
    echo "some_config:some_value" > /tmp/gfs_test_repo/.gfs/TESTDEFAULTCONFIG/config
    tracker=$(gfs config read -p TESTDEFAULTCONFIG issue_tracker)
    assertEquals "jira" "$tracker"
}

oneTimeTearDown() {
    ./test_teardown.sh

    rm -rf ~/.gfs/TESTCONFIG
}

# shellcheck disable=SC1091
. ./shunit2
