#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
release_done_status:3
develop_branch:develop
master_branch:master
" > /tmp/gfs_test_repo/.gfs/TEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST

    # Make develop branch from master
    git checkout -b develop master

    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
    gfs feature start -t "TEST-1" -b "test_feature"
    echo "I made some changes" > testfile
    git add testfile
    git commit -m "TEST-1 made changes"
    gfs feature finish -t "TEST-1"

    gfs release start -t "RTEST-1" -b "test_release"
}

tearDown() {
    test -f testfile && { git rm testfile ; git commit -m "Cleanup" ; }
    git checkout master
    git branch -D test_release
    cd "$SCRIPTDIR" || exit 1
}

testUpdatesIssueToDone() {
    echo "Testing: Updates issue to Done"
    gfs release finish -t "RTEST-1"
    updates_done=$(grep -E '^status:' /tmp/gfs_tracker_mock-RTEST-1)
    assertEquals "status:Done" "$updates_done"

    # Exit the test cleanly regardless of failures
    return 0
}

testPullsIfThereIsARemote() {
    echo "Testing: Pulls if there's a remote branch"
    git init --bare /tmp/gfs_test_repo_remote
    git remote add origin /tmp/gfs_test_repo_remote
    git push --set-upstream origin test_release
    if gfs release finish -t "RTEST-15" 2>&1 | \
        grep -E "Already.up.to.date" > /dev/null ; then
        assertTrue 0
    else
        message="gfs release finish should try to pull remote branches if a "
        message+="remote repo exists"
        fail "$message"
    fi
    rm -rf /tmp/gfs_test_repo_remote
    git remote remove origin

    # Exit the test cleanly regardless of failures
    return 0
}

testAbortsIfMergeIntoMasterFails() {
    echo "Testing: Aborts if merge fails in $(pwd)"
    git checkout master
    echo "I made some conflicting changes" > testfile
    git add testfile
    git commit -m "TEST-2 made conflicting changes"
    git checkout test_release
    echo "I made this hotfix change" > testfile
    git add testfile
    git commit -m "TEST-2 made some hot changes"

    # Make sure it aborts
    if gfs release finish -t "TEST-2" ; then
        fail "gfs_release_finish should fail and abort if there's a conflict"
    else
        assertTrue 0
    fi

    # Make sure it didn't leave the repo in a merge state
    git merge --no-edit HEAD &> /dev/null
    result=$? # 0 means the merge was aborted, 128 means repo is in a merge
    assertEquals "gfs release finish should abort merge" "$result" "0"

    # Exit the test cleanly regardless of failures
    return 0
}

testAbortsIfMergeIntoDevelopFails() {
    echo "Testing: Aborts if merge fails in $(pwd)"
    git checkout develop
    echo "I made some conflicting changes" > testfile
    git add testfile
    git commit -m "TEST-2 made conflicting changes"
    git checkout test_release
    echo "I made this hotfix change" > testfile
    git add testfile
    git commit -m "TEST-2 made some hot changes"

    # Make sure it aborts
    if gfs release finish -t "TEST-2" ; then
        fail "gfs_release_finish should fail and abort if there's a conflict"
    else
        assertTrue 0
    fi

    # Make sure it didn't leave the repo in a merge state
    git merge --no-edit HEAD &> /dev/null
    result=$? # 0 means the merge was aborted, 128 means repo is in a merge
    assertEquals "gfs release finish should abort merge" "$result" "0"

    # Exit the test cleanly regardless of failures
    return 0
}

testSkipsMasterMergeIfAlreadyMerged() {
    # Given a release branch that's already been merged into master
    git checkout master
    git merge --no-edit test_release

    # When gfs release finish is run on the release branch
    git checkout test_release
    gfs release finish -t "RTEST-1"
    result=$?

    # Then the release branch is merged into the develop branch
    message="gfs release finish should succeed even if master branch already "
    message+="merged"
    assertTrue "$message" $result

    # Exit the test cleanly regardless of failures
    return 0
}

testAbortsIfMasterHookFails() {
    echo "Testing: Aborts if hook fails"

    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/release_finish_master
    chmod 755 .gfs/TEST/hooks/release_finish_master

    # Finish the feature
    if gfs_release_finish -t "TEST-3" ; then
        fail "gfs_releasee_finish should fail if its hook fails"
    else
        assertTrue 0
    fi

    rm .gfs/TEST/hooks/release_finish_master

    # Exit the test cleanly regardless of failures
    return 0
}

testAbortsIfDevelopHookFails() {
    echo "Testing: Aborts if hook fails"

    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/release_finish_develop
    chmod 755 .gfs/TEST/hooks/release_finish_develop

    # Finish the release
    if gfs_release_finish -t "TEST-3" ; then
        fail "gfs_release_finish should fail if its hook fails"
    else
        assertTrue 0
    fi

    rm .gfs/TEST/hooks/release_finish_develop

    # Exit the test cleanly regardless of failures
    return 0
}

testChecksOutOriginalBranchIfMasterHookFails() {
    # Given a release_finish_master hook that fails
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/release_finish_master
    chmod 755 .gfs/TEST/hooks/release_finish_master

    # When gfs_release_finish is run on the release branch
    result=0
    gfs_release_finish -t "TEST-3" || result=$?

    # Then gfs_release_finish exits with non-zero exit status ...
    assertFalse "gfs_release_finish should fail if its hook fails" $result

    # ... and the release branch is checked out
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    assertEquals "test_release" "$current_branch_name"

    rm .gfs/TEST/hooks/release_finish_master

    # Exit the test cleanly regardless of failures
    return 0
}

testChecksOutOriginalBranchIfDevelopHookFails() {
    # Given a release_finish_develop hook that fails
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/release_finish_develop
    chmod 755 .gfs/TEST/hooks/release_finish_develop

    # When gfs_release_finish is run on the release branch
    result=0
    gfs_release_finish -t "TEST-3" || result=$?

    # Then gfs_release_finish exits with non-zero exit status ...
    assertFalse "gfs_release_finish should fail if its hook fails" $result

    # ... and the release branch is checked out
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    assertEquals "test_release" "$current_branch_name"

    rm .gfs/TEST/hooks/release_finish_develop

    # Exit the test cleanly regardless of failures
    return 0
}

testPassesTicketToMasterHook() {
    # Given a release_finish_master hook
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    while [ \"\$#\" -gt 0 ] ; do
        if [ \"\$1\" = \"-t\" ] ; then
            shift
            if [ \"\$1\" = \"TEST-3\" ] ; then
                exit 0
            fi
        fi
        shift
    done
    exit 1" \
        > .gfs/TEST/hooks/release_finish_master
    chmod 755 .gfs/TEST/hooks/release_finish_master

    # When gfs_release_finish is run with a ticket ID
    result=0
    gfs_release_finish -t "TEST-3" || result=$?

    # Then the ticket ID is passed with the -t argument to the
    # release_finish_master hook
    assertTrue \
        "gfs_release_finish should pass -t flag to release_finish_master" \
        $result

    # Exit the test cleanly regardless of failures
    return 0
}

oneTimeTearDown() {
    ./test_teardown.sh

    rm -rf ~/.gfs/TESTCONFIG
}

# shellcheck disable=SC1091
. ./shunit2
