#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1

    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
in_development_status:2
master_branch:master
develop_branch:develop
branch_field:tracker_branch_field
" > /tmp/gfs_test_repo/.gfs/TEST/config

    git checkout -b develop

    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    test "$current_branch_name" = "master" || git checkout master

}

tearDown() {
    test -f testfile && { git rm testfile ; git commit -m "Cleanup" ; }

    # Get us back to master if we're not already there
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    test "$current_branch_name" = "master" || git checkout master

    # Delete the "test_feature" branch if it exists
    git rev-parse --verify test_feature >/dev/null 2>&1 && \
        git branch -D test_feature
    cd "$SCRIPTDIR" || exit 1
}

testDoesNotPullIfNoRemote() {
    # Given a feature branch with no remote
    gfs feature start -t "TEST-1" -b "test_feature"

    # When gfs update is run
    rc=0
    output=$(gfs update -y 2>&1)

    # Then it won't git pull or git push
    assertNotContains "$output" "git pull"

    return 0
}

testPullsIfTheresARemote() {
    # Given there's a feature branch with a remote
    gfs feature start -t "TEST-1" -b "test_feature"
    git init --bare /tmp/gfs_test_repo_remote
    git remote add origin /tmp/gfs_test_repo_remote
    git push --set-upstream origin test_feature

    # When gfs update is run
    rc=0
    output=$(gfs update -y 2>&1)

    # Then it will git pull and git push
    assertContains "$output" "git pull"

    # Clean up
    rm -rf /tmp/gfs_test_repo_remote
    git remote remove origin

    return 0
}

testErrorsIfOnMaster() {
    # Given the current branch is master
    git checkout "$(gfs config read master_branch)"

    # When gfs update is run
    rc=0
    gfs update -y || rc=$?

    # Then it throws an error
    assertFalse "gfs update should throw an error if run on master branch" $rc

    return 0
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2
