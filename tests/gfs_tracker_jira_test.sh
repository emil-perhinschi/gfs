#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:jira
tracker_domain:test
" > /tmp/gfs_test_repo/.gfs/TEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd "$SCRIPTDIR" || exit 1
}

# If Jira returns "HTTP/1.1 204", the exit code should be 0
testUpdateFieldReportsSuccess() {
    # -e simulates the exit code api function. Not as accurate as I'd
    # like, but the output is generated in the "api" function, so it tests
    # things that are calling the "api" function pretty well, if not the
    # "api" function itself.
    gfs_tracker_jira -t -e 0 update -f testfield=testvalue -t TEST-123
    result=$?
    assertTrue "gfs_tracker_jira should return true if api returns 0" $result

}

# If Jira returns "HTTP/1.1 500", the exit code should be not 0
testUpdateFieldReportsFailure() {
    gfs_tracker_jira -t -e 22 update -t TEST-123 -f testfield=testvalue
    result=$?
    assertFalse "gfs_tracker_jira should return false if api returns 22" $result
}

testUpdateSendsCorrectParamsToApi() {
    output=$(gfs_tracker_jira -t update -t TEST-123 -f description=testvalue)
    if [[ $output =~ "method: PUT" ]] ; then
        assertTrue 0
    else
        fail "Method should be PUT for field update"
    fi

    if [[ $output =~ "endpoint: issue/TEST-123" ]] ; then
        assertTrue 0
    else
        fail "Endpoint should be issue/TEST-123 for field update"
    fi

    if [[ $output =~ data:.*testvalue ]] ; then
        assertTrue 0
    else
        fail "Data should contain testvalue for field update"
    fi
}

testApiCanReadParameters() {
    # Because this actually broke when api was called from "update"...
    # (Resetting OPTIND=1 was the fix before running getopts, btw)
    output=$(gfs_tracker_jira -t api -e myendpoint -m PUT -d mydata)
    if [[ $output =~ "method: PUT" ]] ; then
        assertTrue 0
    else
        fail "Method should be PUT"
    fi

    if [[ $output =~ "endpoint: myendpoint" ]] ; then
        assertTrue 0
    else
        fail "Endpoint should be myendpoint"
    fi

    if [[ $output =~ data:\ mydata ]] ; then
        assertTrue 0
    else
        fail "Data should be mydata"
    fi
}

oneTimeTearDown() {
    ./test_teardown.sh

    rm -rf ~/.gfs/TESTCONFIG
}

# shellcheck disable=SC1091
. ./shunit2
