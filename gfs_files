#!/usr/bin/env bash
# gfs_files - Output changes made to feature branch
# usage: git checkout feature_branch ; gfs_update [-d]
# Read the man page for this script by running perldoc on it.

# Stop the script if any command exits with non-zero exit status and
# isn't caught (e.g. by && or ||).
set -e

# error "error string"
# Given a string, outputs the string and exits the script with a non-zero
# status.  Used to report errors.  The script will exit before error exits
# with non-zero status, and set -e above will then abort the script.
#
# Example
# do_something || error "something failed!"
#
error() {
    echo "$@" >&2
    exit 1
}

USAGE="usage: $(basename "$0") [-d]"

# Parse command line options.
# Colon after a letter means it has an argument, otherwise it's a flag.
# e.g. "d:" means you expect "-d arg", "v" means you expect "-v".
while getopts hvd OPT; do
    case "$OPT" in
        h)  # help
            perldoc "$0"
            exit 0
            ;;
        v) # script version
            echo "$(basename "$0") version 0.3"
            exit 0
            ;;
        d) # Diffs?
            diffs=1
            ;;
        \?) # Unexpected argument - usage error
            # getopts issues an error message
            echo "$USAGE" >&2
            exit 1
            ;;
    esac
done

# Remove the switches we parsed above.
shift $((OPTIND - 1))

# Script goes here

#feature_branch=`git rev-parse --abbrev-ref HEAD`
develop_branch=$(gfs_config_read develop_branch)

# git diff --name-status gives you just the names (and status) of the files
# that have been changed.  I suppose this script could be called "gfs_diff"
# and take a "-f" argument, but I usually need files more than diffs.
ns=""
test "$diffs" || ns="--name-status"

remote=$(git remote)
test "$remote" && remote="${remote}/"
git diff $ns "${remote}${develop_branch}"...HEAD


: <<'END_OF_DOCS'
=head1 NAME

gfs_files - Output changes made to feature branch

=head1 SYNOPSIS

    git checkout feature_branch
    gfs_files [-d]

=head1 DESCRIPTION

C<gfs_files> outputs a list of files changed in the feature branch since
it was forked from the development branch.  With C<-d>, it outputs diffs.

=head1 OPTIONS

=over

=item -d

Output diffs instead of a list of files.

=back

=cut
END_OF_DOCS
