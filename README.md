# NAME

gfs - Git Flow for Scrum

# SYNOPSIS

    # Get ready to develop in the git repo for myapp with JIRA project APP.
    # (stores config files in ~/myapp, which you can commit for your
    # team to use - or omit "-d ~/myapp" to use gfs by yourself).
    gfs init -d ~/myapp APP

    # Start work on JIRA issue APP-123, creating a new branch named
    # "add_great_ui_button"
    gfs feature start -b add_great_ui_button -t APP-123

    # Edit files
    gfs edit myfile myotherfile

    # Bring in updates from other developers
    gfs update

    # Commit as usual
    git add -A && git commit -m "Fixed everything"

    # Prepare for code review, run sanity checks, update JIRA issue to code
    # review status, assign as defined in config file
    gfs prepare -t APP-123

    # Finish the feature, merge back into develop branch
    gfs feature finish

    # Meanwhile, in the release manager's office...

    # Start a release, tracking its progress in a Jira ticket
    gfs release start -b release_1.5.0 -t APP-321

    # Debug the release
    gfs edit theirfile anotherfile

    # Finish the release
    gfs release finish

# DESCRIPTION

gfs helps you do git flow through a Scrum workflow, tying into your
favorite issue tracker (well, JIRA is what’s currently supported).

Scrum Masters frequently don’t have the in-depth knowledge that the
Professional Scrum Developer training/certification teaches about certain
expectations on the engineering side of Scrum, including separate development
and release processes, the requirement that a Scrum team have all the resources
necessary to complete a feature (e.g.  not including the guy that submits your
app to the App Store or the sysadmin that needs to push your files to
production servers - that’s release), or the expectation of a build and/or CI
server.

Non-Scrum-trained developers sometimes have difficulty learning and following a
Scrum workflow, causing friction on the development team.

gfs helps keep Scrum teams focused on development while smoothly following
Scrum practices by providing the tools to make using the workflow expected by
Scrum easy.  If the team uses the tools and follows the Scrum/git-flow
workflow, they don’t even need to update JIRA.

gfs is written entirely in bash shell following POSIX standards for
maximum compatibility and transparency.  It’s intended to be used on any
size development project, including enterprise systems in which security
is a top concern.  As such, the gfs scripts are simple wrappers around
git commands and use transparent "curl" commands to access JIRA.
Password information is stored in standard .netrc files whose
permissions are set to 600 before data is written to them.

# COMMANDS

Type `man gfs_<command>` (e.g. `man gfs_init`) for info about each command.

    init [-d repo_dir] project_code
        Sets up configuration files for the project/team identified by
        project_code.  Assuming you’re using JIRA, this is the JIRA project
        code.  In some cases (e.g.  when you’re doing things wrong ;-), it
        might be the JIRA issue code for an Epic.  This project will have a
        development branch.  You’ll create feature branches from that
        branch, and push a release from that branch.

    feature_start -b branch_name [-t ticket_code]
        Creates a feature branch to start work on a story. Assigns JIRA
        ticket to user, transitions to "in development" status.

    files [-d]
        Displays list of files modifed in the feature branch. -d flag
        outputs diffs.

    update [-n]
        Merges updates from development branch into a feature branch.

    prepare [-u] [-t jira_code]
        Runs sanity checks on code/story before finishing it.

    feature_finish
        Merges feature branch back into develop branch.

    release_start [-t ticket_code] [-b branch_name]
        Creates a release branch from the develop branch. Assigns JIRA ticket
        to user, transitions into initial "being worked on" status (as
        configured in the project config).

    release_finish
        Merges release branch into master and development branch.

    config_read fieldname
        Outputs the value of field fieldname.

    project_code [project_code|-r]
        Sets current project to project_code. -r returns current project
        code instead.  This means future commands will use the config and
        hooks for project_code.  Normally, you’ll set this once for your
        team’s code.  If you’re switching teams, you’ll use this to switch
        to the new team/project/product’s config.

    checkout [ "develop" | branch_name ]
        "gfs checkout develop" will "git checkout" the development branch (as
        configured for the current project). This is the same as typing
        "git checkout `config_read develop_branch`".
        "gfs checkout branch_name" is the same as "git checkout branch_name",
        allowing you to use "gfs checkout" all the time for convenience.

    edit [-l] [-f feature_name] [-s] [file1] [file2 ...]
        Edits file in your $EDITOR, remembering the list of files for you,
        so you can `gfs edit` and pick up where you left off, and optionally
        specify a feature, so you can `gfs edit -f cart` and have all the
        cart-related files open for you.

# INSTALL

- `git clone` onto your development system, e.g.
    `cd ; git clone https://gitlab.com/ggruen/gfs.git`
- Install gfs scripts
    - `sudo make prefix=/usr/local install` # If you have permissions to do so
    - `make install` # To install in your home directory
- Set up PATH if you installed in your home directory (and don't have bin
  in your PATH already):
    - `echo 'export PATH=$PATH:$HOME/bin' >> ~/.bash_profile`
    - `source ~/.bash_profile`
- Clean up
    - `rm -r ~/gfs`

# FIRST RUN

Set up your repository:

    cd myrepo
    gfs init

OR, if you're joining a team that's already set up their repository to use gfs,
skip `gfs_init` and add your Jira info to ~/.netrc:

    echo "machine JIRA_INSTANCE login JIRA_USERNAME password JIRA_PASSWORD" \
        >> ~/.netrc && chmod 600 ~/.netrc
    $EDITOR ~/.netrc # And fill in the all-caps fields

# UPGRADE

I intend to keep backwards-compatibility, because I use this doing professional
development and expect you do too.

    gfs upgrade

# UNINSTALL

    gfs upgrade -u

If you installed in more than one location, you can specify the path to the
location you used, e.g. `/usr/local/gfs upgrade -u`.

# UNIT TESTS

    cd ~/gfs/tests
    ./run_tests.sh

# JIRA INTEGRATION TESTS

Runs a test suite against your Jira instance and a Jira issue you specify.

*Create a new Jira issue, don't use an existing one*, as the tests will blow
away the description field and add a comment (and not delete it), then specify
its code with the "-i" flag.

    cd ~/gfs/tests
    ./gfs_tracker_jira_integration_test.sh \
            -d myjira.atlassian.net \
            -i issue_code \
            -u test_username

# KNOWN ISSUES

Some of the scripts support the `issue_tracker` config parameter, but not all.
Once all do, gfs will be able to support trakers besides Jira.

# CONTRIBUTING

You can create issues, in the form of user stories, in the GitLab issue tracker.

Pull requests are also welcome (preferably corresponding to an issue).
